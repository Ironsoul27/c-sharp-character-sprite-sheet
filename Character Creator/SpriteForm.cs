﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Character_Creator
{
    public partial class SpriteForm : Form
    {
        SpriteSheet Spritesheet = null;
        Bitmap drawArea = null;

        public SpriteForm()
        {
            InitializeComponent();
            drawArea = new Bitmap(comboBoxSheets.Width, comboBoxSheets.Height);
        }

        private void SpriteForm_Activated(object sender, EventArgs e)
        {
            MdiClient parent = Parent as MdiClient;
            if (Parent != null)
            {
                foreach(Form child in parent.MdiChildren)
                {
                    if (child.GetType() == typeof(SpriteSheetForm))
                    {
                        SpriteSheetForm sheet = child as SpriteSheetForm;
                        SpriteSheet ss = sheet.Spritesheet;
                        if (ss != null && !comboBoxSheets.Items.Contains(ss))
                        {
                            comboBoxSheets.Items.Add(ss);
                        }
                    }
                }
            }

            if (Spritesheet != null)
            {
                comboBoxSheets.SelectedItem = Spritesheet;
            }
            else if (comboBoxSheets.Items.Count > 0)
            {
                comboBoxSheets.SelectedIndex = 0;
                Spritesheet = comboBoxSheets.SelectedItem as SpriteSheet;
            }
        }

        SpriteSheetForm FindSheet()
        {
            MdiClient parent = Parent as MdiClient;
            if (parent != null)
            {
                foreach (Form child in parent.MdiChildren)
                {
                    if (child.GetType() == typeof(SpriteSheetForm))
                    {
                        SpriteSheetForm sheet = child as SpriteSheetForm;
                        if (sheet.Spritesheet == Spritesheet)
                            return sheet;
                    }
                }
            }
            return null;
        }

        private void AddButton_Click(object sender, EventArgs e)
        {
            if (Spritesheet != null)
            {
                SpriteSheetForm sheet = FindSheet();
                if (sheet != null)
                {
                    listBoxTiles.Items.Add(sheet.CurrentTile);

                    DrawCharacter();
                }
            }
        }

        private void DrawCharacter()
        {
            Graphics g = Graphics.FromImage(drawArea);
            g.FillRectangle(Brushes.White, 0, 0, drawArea.Width, drawArea.Height);

            Rectangle dest = new Rectangle(0, 0,
                Spritesheet.GridWidth << 2, Spritesheet.GridHeight << 2);

            foreach(Point tile in listBoxTiles.Items)
            {
                Rectangle source = new Rectangle(
                    tile.X * (Spritesheet.GridWidth + Spritesheet.Spacing),
                    tile.Y * (Spritesheet.GridHeight + Spritesheet.Spacing),
                    Spritesheet.GridWidth,
                    Spritesheet.GridHeight);

                g.DrawImage(Spritesheet.Image, dest, source, GraphicsUnit.Pixel);
            }
            g.Dispose();

            pictureBox.Image = drawArea;
        }
    }
}
