﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Character_Creator
{
    public partial class SpriteSheetForm : Form
    {
        public SpriteSheet Spritesheet { get; private set; }
        Bitmap drawArea;

        public Point CurrentTile { get; private set; } = new Point();

        public string Filename
        {
            get { return (Spritesheet != null) ?
                  Spritesheet.Filename : string.Empty;
            }
        }

        //int gridWidth = 16;
        //int gridHeight = 16;
        //int spacing = 1;

        public SpriteSheetForm()
        {
            InitializeComponent();

            drawArea = new Bitmap(pictureBox1.Width, pictureBox1.Height);
        }


        private void DrawGrid()
        {
            pictureBox1.DrawToBitmap(drawArea, pictureBox1.Bounds);

            Graphics g;
            g = Graphics.FromImage(drawArea);

            g.Clear(Color.White);

            if (Spritesheet == null)
                return;

            g.DrawImage(Spritesheet.Image, 0, 0);

            //if (Spritesheet != null)
            //{
            //    g.DrawImage(Spritesheet.Image, 0, 0);
            //}

            Pen pen = new Pen(Brushes.Black);

            int height = pictureBox1.Height;
            int width = pictureBox1.Width;

            for (int y = 0; y < height; y += Spritesheet.GridHeight + Spritesheet.Spacing)
            {
                g.DrawLine(pen, 0, y, width, y);
            }

            for (int x = 0; x < width; x += Spritesheet.GridWidth + Spritesheet.Spacing)
            {
                g.DrawLine(pen, x, 0, x, height);
            }

            Pen highlight = new Pen(Brushes.Red);
            g.DrawRectangle(highlight,
               CurrentTile.X * (Spritesheet.GetWidth + Spritesheet.Spacing),
               CurrentTile.Y * (Spritesheet.GetHeight + Spritesheet.Spacing),
               Spritesheet.GridWidth + Spritesheet.Spacing,
               Spritesheet.GridHeight + Spritesheet.Spacing);

            g.Dispose();

            pictureBox1.Image = drawArea;
        }

        private void TextBoxWidth_TextChanged(object sender, EventArgs e)
        {
            int width;
            if (int.TryParse(textBoxWidth.Text, out width) == true)
            {
                Spritesheet.GridWidth = width;
                DrawGrid();
            }
            textBoxWidth.Text = Spritesheet.GridWidth.ToString();
        }

        private void TextBoxHeight_TextChanged_1(object sender, EventArgs e)
        {
            int height;
            if (int.TryParse(textBoxHeight.Text, out height) == true)
            {
                Spritesheet.GridHeight = height;
                DrawGrid();
            }
            textBoxHeight.Text = Spritesheet.GetHeight.ToString();
        }

        private void textBoxSpacing_TextChanged_1(object sender, EventArgs e)
        {
            int spacing;
            if (int.TryParse(textBoxSpacing.Text, out spacing) == true)
            {
                Spritesheet.Spacing = spacing;
                DrawGrid();
            }

            textBoxSpacing.Text = Spritesheet.Spacing.ToString();
        }

        private void SpriteSheetForm_Shown(object sender, EventArgs e)
        {
            DrawGrid();
        }

        private void LoadButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                if (dlg.CheckFileExists == true)
                {
                    Spritesheet = new SpriteSheet(dlg.FileName);
                    DrawGrid();
                }
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (Spritesheet == null)
                return;

            if (e.GetType() == typeof(MouseEventArgs))
            {
                MouseEventArgs mouse = e as MouseEventArgs;
                CurrentTile = new Point(
                    mouse.X / (Spritesheet.GridWidth + Spritesheet.Spacing),
                    mouse.Y / (Spritesheet.GridHeight + Spritesheet.Spacing));
                DrawGrid();
            }
        }
    }
}
